/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXRUN_USERCHROOT
#define INCLUDED_BUILDBOXRUN_USERCHROOT

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_runner.h>

namespace buildboxcommon {
namespace buildboxrun {
namespace userchroot {

class UserChrootRunner : public Runner {
  public:
    /**
     * The simplest possible working runner: runs a job directly on your
     * machine as the current user without making any attempt to sandbox it.
     */
    ActionResult execute(const Command &command,
                         const Digest &inputRootDigest) override;

  private:
};

} // namespace userchroot
} // namespace buildboxrun
} // namespace buildboxcommon
#endif
