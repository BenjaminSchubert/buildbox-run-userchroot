cmake_minimum_required(VERSION 3.6)
set(CMAKE_CXX_STANDARD 14)

project(buildbox-run-userchroot C CXX)

if(${CMAKE_SYSTEM_NAME} MATCHES "AIX" AND ${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
    set(CMAKE_NO_SYSTEM_FROM_IMPORTED ON)
endif()

find_package(BuildboxCommon REQUIRED)

add_executable(buildbox-run-userchroot
    buildboxrun-userchroot/buildboxrun_userchroot.cpp
    buildboxrun-userchroot/buildboxrun_userchroot.m.cpp
)
if(CMAKE_BUILD_TYPE STREQUAL "DEBUG")
    set(DEBUG_FLAGS -Werror -Wextra -pedantic-errors -Wconversion -Wno-vla)
endif()
if(${CMAKE_SYSTEM_NAME} MATCHES "AIX" AND ${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
    message("Skipping all warnings due to GNU compiler + AIX system")
else()
    target_compile_options(buildbox-run-userchroot PRIVATE -Wall -Werror=shadow ${DEBUG_FLAGS})
endif()
install(TARGETS buildbox-run-userchroot RUNTIME DESTINATION bin)
target_link_libraries(buildbox-run-userchroot Buildbox::buildboxcommon)
target_include_directories(buildbox-run-userchroot PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/buildboxrun-userchroot)

include(CTest)
if(BUILD_TESTING)
    add_subdirectory(test)
endif()
